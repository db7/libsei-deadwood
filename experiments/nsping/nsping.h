
#ifndef NSPING_INCLUDED
#define NSPING_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include "dns-lib.h"
#include "dns-rr.h"


#ifdef sys5
typedef unsigned long u_int32_t;
typedef unsigned short u_int16_t;
#define INADDR_NONE -1
#endif

#define QUERY_BACKLOG	1024
#define DNS_PORT		53
#define DEFAULT_SECOND_INTERVAL 0	
#define DEFAULT_USECOND_INTERVAL	10

int guess_zone(void);
struct timeval *set_timer(char *timearg);
void probe(int sig);
int dns_packet(u_char **qp, int id);
void handle_incoming(void);
void update(u_char *bp, int l);
void summarize(int);
double trip_time(struct timeval *send_time, struct timeval *rcv);
struct timeval *timeval_subtract(struct timeval *out, struct timeval *in);
char *addr_string(u_int32_t address);
int bind_udp_socket(u_int32_t address, u_int16_t port);
void ddprintf(char *fmt, ...);
void usage(void);
char *xstrdup(char *v);
u_int32_t resolve(char *name);

#endif
