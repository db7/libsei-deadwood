# SEI-hardened Deadwood 3.2.05

A modified version of Deadwood hardened against data corruptions.

Create a file `Makefile.local` and add the following line to the file:

    SEI_HOME = /path/to/libsei

Then type `make` to compile. The binary is placed in `build/`.

## Dependencies

* libsei, http://bitbucket.org/db7/libsei
* gcc == 4.7